import {HOST} from "./Server";
import CompoundDocument from '../src/CompoundDocument';
import {Resource} from "../src";
describe('CompoundDocument test suite', () => {
    test('Creating CompoundDocument', async () => {
        const instance = new CompoundDocument('');
        expect(instance).toBeInstanceOf(CompoundDocument);
    })
    test('Fetch All Resources', async () => {
        const doc = new CompoundDocument<Resource>(`${HOST}/resources`);
        await doc.self();
        expect(doc.data).toBeInstanceOf(Array)
        expect(doc.data).toHaveLength(1)
        expect((doc.data as Resource[])[0]).toMatchObject({type: 'resources', id: 1, attributes: {foo: 'bar'}})
    })
    test('Fetch One Resource', async () => {
        const doc = new CompoundDocument<Resource>(`${HOST}/resources/1`);
        await doc.self();
        expect(doc.data).toMatchObject({type: 'resources', id: 1, attributes: {foo: 'bar'}})
    })
    test('Create Resource', async () => {
        const doc = new CompoundDocument<Resource>(`${HOST}/resources`);
        await doc.create({type: 'resources', id: '1', attributes: {foo: 'bar'}});
        expect(doc.data).toMatchObject({
            jsonapi: {version: '1.0'},
            data: {type: 'resources', id: '1', attributes: {foo: 'bar'}}
        })
    })
    test('Update Resource', async () => {
        const doc = new CompoundDocument<Resource>(`${HOST}/resources/1`);
        await doc.update({type: 'resources', id: '1', attributes: {foo: 'bar'}});
        expect(doc.data).toMatchObject({
            jsonapi: {version: '1.0',},
            data: {type: 'resources', id: '1', attributes: {foo: 'bar'}}
        })
    })
    test('Delete Resource', async () => {
        const doc = new CompoundDocument<Resource>(`${HOST}/resources/1`);
        await doc.delete();
        expect(doc.data).toBeNull();
    })
});
