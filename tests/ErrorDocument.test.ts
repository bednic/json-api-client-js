import ErrorDocument from '../src/ErrorDocument';

describe('ErrorDocumentTest', () => {
    test('testConstructor', () => {
        try {
            const response: any = {
                "errors": [
                    {
                        "status": "422",
                        "source": { "pointer": "/data/attributes/firstName" },
                        "title": "Invalid Attribute",
                        "detail": "First name must contain at least three characters."
                    }
                ]
            }
            throw new ErrorDocument(response.errors)
        } catch (err: any) {
            expect(err).toBeInstanceOf(ErrorDocument);
            expect(err.jsonapi.version).toBe('1.0')
            expect(err.errors[0].title).toBe('Invalid Attribute');
            expect(err.errors[0].status).toBe("422");
        }
    });
});
