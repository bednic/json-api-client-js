import Filter from '../src/filter/Filter';

describe('FilterTest', () => {
    test('testSparseFieldsFor', () => {
        const f = new Filter();
        f.sparseFieldsFor('resource', ['field1', 'field2']);
        const q = f.toQuery();
        expect(q.toString()).toBe('?fields[resource]=field1,field2');
    });
});
