import Value from '../../src/filter/Value';
import InvalidArgument from '../../src/error/InvalidArgument';

describe('ValueTest', () => {
    test('testConstructor', () => {
        expect(() => new Value(undefined)).toThrowError(InvalidArgument);
    });
    test('testNull', () => {
        expect(new Value(null).express()).toBe('null');
    });
    test('testDate', () => {
        expect(new Value(new Date('2020-01-01')).express()).toBe('datetime\'2020-01-01T00:00:00.000Z\'');
    });
    test('testString', () => {
        expect(new Value('string').express()).toBe('\'string\'');
    });
    test('testNumber', () => {
        expect(new Value(123.123).express()).toBe('123.123');
        expect(new Value(0).express()).toBe('0');
        expect(new Value(123).express()).toBe('123');
        expect(new Value(-1).express()).toBe('-1');
    });
    test('testBoolean', () => {
        expect(new Value(true).express()).toBe('true');
        expect(new Value(false).express()).toBe('false');
    });
    test('testArray', () => {
        expect(new Value([1,'a',true]).express()).toBe('(1,\'a\',true)');
    });
});
