import fetchMock, {enableFetchMocks, MockResponseInitFunction} from 'jest-fetch-mock'

enableFetchMocks();
const HOST = 'https://test.com';

const response: MockResponseInitFunction = async (request) => {
    if (!request.headers.has('Accept')
        || request.headers.get('Accept') !== 'application/vnd.api+json') {
        fail('Headers are incorrect');
    }
    if (!request.headers.has('Content-Type')
        || request.headers.get('Content-Type') !== 'application/vnd.api+json') {
        fail('Headers are incorrect');
    }
    if (request.url.endsWith('/resources')) {
        if (request.method === 'POST') {
            const data: object = await request.json();
            const body = JSON.stringify({data: data});
            return {
                status: 201,
                body: body
            }
        } else {
            return {
                status: 200,
                body: JSON.stringify({
                    jsonapi: {version: '1.0'},
                    data: [{
                        type: 'resources',
                        id: 1,
                        attributes: {
                            foo: 'bar'
                        }
                    }]
                })
            }
        }
    } else if (request.url.endsWith('/resources/1')) {
        if (request.method === 'PATCH') {
            return {
                status: 200,
                body: JSON.stringify({
                    jsonapi: '1.0',
                    data: await request.json()
                })
            }
        } else if (request.method === 'GET') {
            return {
                status: 200,
                body: JSON.stringify({
                    jsonapi: {version: '1.0'},
                    data: {
                        type: 'resources',
                        id: 1,
                        attributes: {
                            foo: 'bar'
                        }
                    }
                })
            }
        } else if (request.method === 'DELETE') {
            return {
                status: 200
            }
        } else {
            return {
                status: 405,
                body: JSON.stringify({
                    jsonapi: {version: '1.0'},
                    errors: [{
                        title: 'Method Not Allowed',
                        status: 405
                    }]
                })
            }
        }
    } else if (request.url.endsWith('/foo')) {
        return {
            status: 404,
            body: JSON.stringify({
                errors: [{
                    title: 'Not found',
                    status: 404
                }]
            })
        }
    } else {
        return {
            status: 500,
            body: JSON.stringify({
                errors: [{
                    title: 'Server Error',
                    status: 500
                }]
            })
        }
    }
};
fetchMock.mockIf(/^https?:\/\/test.com.*$/, response)

export {HOST};
