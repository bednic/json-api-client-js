# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0](https://gitlab.com/bednic/json-api-client-js/compare/0.2.13...0.4.0) (2022-05-30)


### ⚠ BREAKING CHANGES

* Yes

### Changed

* Change from Axios to Fetch API ([f44705d](https://gitlab.com/bednic/json-api-client-js/commit/f44705ddec1bcff0403a82c22496df92739d6c28))


### Fixed

* Fix issue [#10](https://gitlab.com/bednic/json-api-client-js/issues/10) ([27c8b0d](https://gitlab.com/bednic/json-api-client-js/commit/27c8b0d2bd38dfe0e272c9311ba73b06f0eae5d4))

## [0.3.0](https://gitlab.com/bednic/json-api-client-js/compare/0.2.13...0.3.0) (2022-05-26)


### ⚠ BREAKING CHANGES

* Yes

### Changed

* Change from Axios to Fetch API ([f44705d](https://gitlab.com/bednic/json-api-client-js/commit/f44705ddec1bcff0403a82c22496df92739d6c28))

### [0.2.13](https://gitlab.com/bednic/json-api-client-js/compare/0.2.12...0.2.13) (2022-02-07)

### [0.2.12](https://gitlab.com/bednic/json-api-client-js/compare/0.2.11...0.2.12) (2021-11-01)

### [0.2.11](https://gitlab.com/bednic/json-api-client-js/compare/0.2.10...0.2.11) (2021-11-01)

### [0.2.10](https://gitlab.com/bednic/json-api-client-js/compare/0.2.9...0.2.10) (2021-08-17)


### Fixed

* DELETE HTTP request does not provide Content-type header ([b29be66](https://gitlab.com/bednic/json-api-client-js/commit/b29be667da95b8efcd356a475072a548c2595b71))

### [0.2.9](https://gitlab.com/bednic/json-api-client-js/compare/0.2.8...0.2.9) (2021-04-22)


### Fixed

* ::getRelation and ::getRelationship did not inherit client instance ([c7b8968](https://gitlab.com/bednic/json-api-client-js/commit/c7b896869c9c725e29e9b3592e77c706730689d1))

### [0.2.8](https://gitlab.com/bednic/json-api-client-js/compare/0.2.7...0.2.8) (2021-04-13)


### Fixed

* implements rest of string and date function to ExpressionBuilder ([692e7d4](https://gitlab.com/bednic/json-api-client-js/commit/692e7d4d02dd4d011e3f54f68b5bce6f28a09587))

### [0.2.7](https://gitlab.com/bednic/json-api-client-js/compare/0.2.6...0.2.7) (2021-04-12)


### Fixed

* remove mistakenly added opt param to create and update methods ([676b0fc](https://gitlab.com/bednic/json-api-client-js/commit/676b0fc917335779c944a67499928cbdbe8bf4af))

### [0.2.6](https://gitlab.com/bednic/json-api-client-js/compare/0.2.5...0.2.6) (2021-04-12)


### Changed

* rename read back to self ([9698da1](https://gitlab.com/bednic/json-api-client-js/commit/9698da1c0407f14eb47d39091f13ed43bdb7290c))

### [0.2.5](https://gitlab.com/bednic/json-api-client-js/compare/0.2.4...0.2.5) (2021-04-12)


### Fixed

* fix unnecessary incompatibility ([103574d](https://gitlab.com/bednic/json-api-client-js/commit/103574d639248147e8a57573b735186f149e34a2))

### [0.2.4](https://gitlab.com/bednic/json-api-client-js/compare/0.2.3...0.2.4) (2021-04-12)


### Changed

* create and update now accept Partial to enable typed patching ([f6e16c2](https://gitlab.com/bednic/json-api-client-js/commit/f6e16c2a989ee1496680d87a6f82d08ab15299a6))

### [0.2.3](https://gitlab.com/bednic/json-api-client-js/compare/0.2.2...0.2.3) (2021-03-31)


### Fixed

* add missing exports for errors ([6ee1291](https://gitlab.com/bednic/json-api-client-js/commit/6ee1291341c68d7e08a940fae88e75564d0e88ce))

### [0.2.2](https://gitlab.com/bednic/json-api-client-js/compare/0.2.1...0.2.2) (2021-03-11)


### Fixed

* fix not operator back to unary ([82bd7e4](https://gitlab.com/bednic/json-api-client-js/commit/82bd7e44859b17ec513f6f97750a5c72f6aeb7ae))

### [0.2.1](https://gitlab.com/bednic/json-api-client-js/compare/0.2.0...0.2.1) (2021-03-10)


### Fixed

* fix not operand expression ([dcb5af5](https://gitlab.com/bednic/json-api-client-js/commit/dcb5af5454e502182991aeecee4a6274589298d3))

## [0.2.0](https://gitlab.com/bednic/json-api-client-js/compare/0.1.4...0.2.0) (2021-03-10)


### ⚠ BREAKING CHANGES

* Rename methods

### Fixed

* Missing Link implementation ([ef56ec4](https://gitlab.com/bednic/json-api-client-js/commit/ef56ec4ff0dccf0c1065460d12c5cec1ae9ce100))
* On network error compound document throws type TypeError: Cannot read property 'data' of undefined ([e8defa4](https://gitlab.com/bednic/json-api-client-js/commit/e8defa457a10e62332c49538ed97cd686a738154)), closes [#5](https://gitlab.com/bednic/json-api-client-js/issues/5)


### Changed

* ResourceIdentifier now extends Object ([97a03dd](https://gitlab.com/bednic/json-api-client-js/commit/97a03ddbf5477c010dd58d01bb84937b3304143e))
* Several changes ([a430ce2](https://gitlab.com/bednic/json-api-client-js/commit/a430ce21b78898f18c165e8719e3f2cdea60b28c))


### Added

* Add has function to PrettyExpresssionBuilder ([f832863](https://gitlab.com/bednic/json-api-client-js/commit/f8328630bcf25d8fe986e8890592898c898a4790))

### [0.1.4](https://gitlab.com/bednic/json-api-client-js/compare/0.1.3...0.1.4) (2021-01-18)

### [0.1.3](https://gitlab.com/bednic/json-api-client-js/compare/0.1.2...0.1.3) (2021-01-18)


### Fixed

* CompoundDocument throw ErrorDocument when error occurs ([8089811](https://gitlab.com/bednic/json-api-client-js/commit/8089811fe3de70df80c7852ef07ddce6d61da95f))
* Value ([d87d25c](https://gitlab.com/bednic/json-api-client-js/commit/d87d25c1349e066388d2e3bc395d1432a8f56ef6))

### [0.1.2](https://gitlab.com/bednic/json-api-client-js/compare/0.1.1...0.1.2) (2021-01-11)


### Changed

* Refactor errors handling + adding tests ([64dda9a](https://gitlab.com/bednic/json-api-client-js/commit/64dda9a9027edb657810d560367141123e8adc2d))
* Refactor errors handling + adding tests ([29ace41](https://gitlab.com/bednic/json-api-client-js/commit/29ace412d3d9831beebffb06b969591db1d59804))


### Fixed

* add ErrorDocument to exported classes from index ([b89b1bd](https://gitlab.com/bednic/json-api-client-js/commit/b89b1bd12d91be7101b85d25b1b93be8decec482))
* ErrorObject ([1420ae1](https://gitlab.com/bednic/json-api-client-js/commit/1420ae134ac41bda343747bcbe40b932db04b853))

### [0.1.1](https://gitlab.com/bednic/json-api-client-js/compare/0.1.0...0.1.1) (2020-10-05)


### Fixed

* Bad constant use ([461200d](https://gitlab.com/bednic/json-api-client-js/commit/461200d42cee21de6dc59a787b8e7af05eb77543)), closes [#1](https://gitlab.com/bednic/json-api-client-js/issues/1)

# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
