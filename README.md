# JSON API Client JS

JSON API Client side JavaScript library to consume APIs

## Usage
```typescript
const doc = new Document<Resource>(`/resources/${id}`);
doc.filter().sparseFieldsFor('resource', ['name', 'desc']);
await doc.self();
const resource = doc.data as Resource;
```
