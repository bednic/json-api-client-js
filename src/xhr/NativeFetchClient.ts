export default class NativeFetchClient implements Client {
    private readonly abortController!: AbortController;
    private readonly reqInit!: RequestInit;
    private readonly headers!: Headers;

    constructor(init?: RequestInit) {
        this.abortController = new AbortController();
        this.headers = new Headers();
        this.reqInit = Object.assign({
            method: 'GET',
            signal: this.abortController.signal,
            headers: this.headers
        }, init);
    }

    abort(): void {
        this.abortController.abort();
    }

    delete(url: string): Promise<Response> {

        this.reqInit.method = 'DELETE';
        return fetch(url, this.reqInit);
    }

    get(url: string): Promise<Response> {
        return fetch(url, this.reqInit);
    }

    patch(url: string, body: BodyInit): Promise<Response> {
        this.reqInit.method = 'PATCH';
        this.reqInit.body = body;
        return fetch(url, this.reqInit);
    }

    post(url: string, body: BodyInit): Promise<Response> {
        this.reqInit.method = 'POST';
        this.reqInit.body = body;
        return fetch(url, this.reqInit);
    }

    setHeader(name: string, value: string): void {
        this.headers.set(name, value);
    }
}
