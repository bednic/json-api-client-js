import Links from './Links';
import Meta from './Meta';

export default interface ResourceIdentifier extends Object {
    id: string;
    type: string;
    links?: Links;
    meta?: Meta;
}
