interface Client {

    get(url: string): Promise<Response>;

    post(url: string, body: BodyInit): Promise<Response>;

    patch(url: string, body: BodyInit): Promise<Response>;

    delete(url: string): Promise<Response>;

    setHeader(name: string, value: string): void;

    abort(): void;
}
