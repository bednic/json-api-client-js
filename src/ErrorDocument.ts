import ErrorObject from './ErrorObject';
import Meta from './Meta';
import Links from './Links';

export default class ErrorDocument {
    readonly jsonapi: { version: string } = { version: '1.0' };
    errors: ErrorObject[];
    meta?: Meta;
    links?: Links;

    constructor(errors: ErrorObject[], meta?: Meta, links?: Links) {
        this.errors = errors;
        this.meta = meta;
        this.links = links;
    }
}
