import Meta from './Meta';

export default interface Link {
    href: string,
    meta?: Meta
}
