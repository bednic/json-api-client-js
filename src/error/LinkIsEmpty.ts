import JsonApiError from './JsonApiError';

export default class LinkIsEmpty extends JsonApiError{

    constructor(name: string) {
        super();
        this.message = `Link ${name} does not exist or is empty.`
    }
}
