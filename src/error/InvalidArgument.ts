import JsonApiError from './JsonApiError';

export default class InvalidArgument extends JsonApiError{
    name = 'Invalid Argument Exception'
    constructor(arg:string,expected: string, supplied: string) {
        super();
        this.message = `Invalid argument passed to ${arg}. Expected ${expected}, but received ${supplied}`;
    }
}
