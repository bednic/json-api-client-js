import Expression, { Constants } from './Expression';

export default class BinaryExpression implements Expression {
    private left: Expression;
    private right: Expression;
    private operator: Constants;

    constructor(left: Expression, operator: Constants, right: Expression) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    express(): string {
        return `${this.left.express()} ${this.operator} ${this.right.express()}`;
    }
}
