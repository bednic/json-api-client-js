import Expression from './Expression';
import InvalidArgument from '../error/InvalidArgument';

export default class Value implements Expression {
    private value?: string;

    constructor(value: any) {
        if (value === undefined) {
            throw new InvalidArgument('value', ' null, Date, String, Number, Boolean, Array ', typeof value)
        }
        if (value === null) {
            this.value = 'null';
        } else {
            if (value instanceof Date) {
                this.value = `datetime'${value.toISOString()}'`;
            } else if (value instanceof Array) {
                value = value.map(item => new Value(item).express());
                this.value = `(${value.join(',')})`;
            } else if (typeof value === 'string') {
                this.value = `'${value.replace(`'`, `''`)}'`;
            } else if (typeof value === 'boolean') {
                this.value = value ? 'true' : 'false';
            } else if (typeof value === 'number') {
                this.value = value.toString();
            } else {
                throw new InvalidArgument('value', ' null, Date, String, Number, Boolean, Array ', typeof value)
            }
        }
    }

    express(): string {
        return this.value as string;
    }
}
