import Expression, { Constants } from './Expression';

export default class UnaryExpression implements Expression {
    private value: Expression;
    private operator: Constants;

    constructor(value: Expression, operator: Constants) {
        this.value = value;
        this.operator = operator;
    }

    express(): string {
        return `${this.operator} ${this.value.express()}`;
    }
}
