import Expression from './Expression';

export default class Field implements Expression {
    private name: string;

    constructor(name: string) {
        this.name = name;
    }

    express(): string {
        return this.name;
    }
}
