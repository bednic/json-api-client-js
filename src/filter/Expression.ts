export enum Constants {
    // LOGICAL
    LOGICAL_EQUAL = 'eq',
    LOGICAL_NOT_EQUAL = 'ne',
    LOGICAL_GREATER_THAN = 'gt',
    LOGICAL_GREATER_THAN_OR_EQUAL = 'ge',
    LOGICAL_LOWER_THAN = 'lt',
    LOGICAL_LOWER_THAN_OR_EQUAL = 'le',
    LOGICAL_AND = 'and',
    LOGICAL_OR = 'or',
    LOGICAL_NOT = 'not',
    LOGICAL_HAS = 'has',
    LOGICAL_IN = 'in',

    // ARITHMETIC
    ARITHMETIC_ADDITION = 'add',
    ARITHMETIC_SUBTRACTION = 'sub',
    ARITHMETIC_NEGATION = '-',
    ARITHMETIC_MULTIPLICATION = 'mul',
    ARITHMETIC_DIVISION = 'div',
    ARITHMETIC_MODULO = 'mod',

    // FUNCTION
    /* String and Collection */
    FUNCTION_STARTS_WITH = 'startsWith',
    FUNCTION_ENDS_WITH = 'endsWith',
    FUNCTION_CONTAINS = 'contains',
    FUNCTION_CONCAT = 'concat',
    FUNCTION_INDEX_OF = 'indexof',
    FUNCTION_LENGTH = 'length',
    FUNCTION_SUBSTRING = 'substring',

    /* String */
    FUNCTION_MATCHES_PATTERN = 'matchesPattern',
    FUNCTION_TO_LOWER = 'tolower',
    FUNCTION_TO_UPPER = 'toupper',
    FUNCTION_TRIM = 'trim',

    /* Lambda operators */
    FUNCTION_ANY = 'any',
    FUNCTION_ALL = 'all',

    /* Date & time */
    FUNCTION_DATE = 'date',
    FUNCTION_DAY = 'day',
    FUNCTION_HOUR = 'hour',
    FUNCTION_MINUTE = 'minute',
    FUNCTION_MONTH = 'month',
    FUNCTION_NOW = 'now',
    FUNCTION_SECOND = 'second',
    FUNCTION_TIME = 'time',
    FUNCTION_YEAR = 'year',

    /* Arithmetic */
    FUNCTION_CEILING = 'ceiling',
    FUNCTION_FLOOR = 'floor',
    FUNCTION_ROUND = 'round'
}

export default interface Expression {
    express(): string;
}
