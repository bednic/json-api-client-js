import Expression, { Constants } from './Expression';

export default class GroupExpression implements Expression {
    private expressions: Expression[] = [];
    private operator: Constants;

    constructor(operator: Constants, expressions: Expression[] = []) {
        this.expressions = expressions;
        this.operator = operator;
    }

    public addExpression(expression: Expression) {
        this.expressions.push(expression);
    }

    express(): string {
        return `(${this.expressions.map(item => item.express()).join(` ${this.operator} `)})`;
    }
}
