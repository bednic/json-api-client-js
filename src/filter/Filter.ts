import Expression from './Expression';
import ExpressionBuilder from './ExpressionBuilder';

export default class Filter {
    private fields: { [key: string]: string[] } = {};
    private limit: number | null = null;
    private offset: number | null = null;
    private inclusion: string[] = [];
    private expression: Expression | null = null;
    private sortBy: { [key: string]: boolean } = {};

    public sparseFieldsFor(type: string, fields: string[]): this {
        fields = fields.concat(this.fields[type] || []);
        this.fields[type] = fields
        return this;
    }

    public fullFieldsFor(type: string): this {
        delete this.fields[type];
        return this;
    }

    public include(path: string): this {
        this.inclusion.push(path);
        return this;
    }

    public exclude(path: string): this {
        this.inclusion = this.inclusion.filter(item => item != path);
        return this;
    }

    public addSortBy(field: string, desc = false): this {
        this.sortBy[field] = desc;
        return this;
    }

    public removeSortBy(field: string): this {
        delete this.sortBy[field];
        return this;
    }

    public setLimit(limit: number | null): this {
        this.limit = limit;
        return this;
    }

    public setOffset(offset: number | null): this {
        this.offset = offset;
        return this;
    }

    /**
     * Use null to clear filter
     * @param expression
     */
    public where(expression: Expression | null): this {
        this.expression = expression;
        return this;
    }

    public andWhere(expression: Expression): this {
        if (!this.expression) {
            this.where(expression);
        } else {
            this.expression = ExpressionBuilder.and(this.expression, expression);
        }
        return this;
    }

    public orWhere(expression: Expression): this {
        if (!this.expression) {
            this.where(expression);
        } else {
            this.expression = ExpressionBuilder.or(this.expression, expression);
        }
        return this;
    }

    /**
     * Transforms data to axios params object
     */
    public toParams(): { [key: string]: string } {
        const params: { [key: string]: string } = {};
        if (this.limit !== null) {
            params['page[limit]'] = this.limit.toString();
        }
        if (this.offset !== null) {
            params['page[offset]'] = this.offset.toString();
        }
        for (const field in this.fields) {
            params[`fields[${field}]`] = this.fields[field].join(',');
        }
        if (this.expression) {
            params['filter'] = this.expression.express();
        }
        if (this.inclusion.length > 0) {
            params['include'] = this.inclusion.join(',');
        }
        const sort = []
        for (const field in this.sortBy) {
            const desc = this.sortBy[field];
            sort.push(`${desc ? '-' : ''}${field}`)
        }
        if (sort.length) {
            params['sort'] = sort.join(',');
        }
        return params;
    }

    /**
     * @deprecated
     */
    public toString(): string {
        let query = '';
        let glue = '?'
        if (this.limit !== null) {
            query += glue + 'page[limit]=' + this.limit.toString();
            glue = '&';
        }
        if (this.offset !== null) {
            query += glue + 'page[offset]=' + this.offset.toString();
            glue = '&';
        }
        for (const field in this.fields) {
            query += glue + `fields[${field}]=` + this.fields[field].join();
            glue = '&';
        }
        if (this.expression) {
            query += glue + 'filter=' + this.expression.express();
            glue = '&';
        }
        if (this.inclusion.length > 0) {
            query += glue + 'include=' + this.inclusion.join();
            glue = '&';
        }
        const sort = []
        for (const field in this.sortBy) {
            const desc = this.sortBy[field];
            sort.push(`${desc ? '-' : ''}${field}`)
        }
        if (sort.length) {
            query += glue + 'sort=' + sort.join(',');
        }
        return query;
    }
    public toQuery(): string {
        let query = '';
        let glue = '?'
        if (this.limit !== null) {
            query += glue + 'page[limit]=' + this.limit.toString();
            glue = '&';
        }
        if (this.offset !== null) {
            query += glue + 'page[offset]=' + this.offset.toString();
            glue = '&';
        }
        for (const field in this.fields) {
            query += glue + `fields[${field}]=` + this.fields[field].join();
            glue = '&';
        }
        if (this.expression) {
            query += glue + 'filter=' + this.expression.express();
            glue = '&';
        }
        if (this.inclusion.length > 0) {
            query += glue + 'include=' + this.inclusion.join();
            glue = '&';
        }
        const sort = []
        for (const field in this.sortBy) {
            const desc = this.sortBy[field];
            sort.push(`${desc ? '-' : ''}${field}`)
        }
        if (sort.length) {
            query += glue + 'sort=' + sort.join(',');
        }
        return query;
    }
}
