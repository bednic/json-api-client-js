import Expression, { Constants } from './Expression';
import Value from './Value';
import Field from './Field';
import BinaryExpression from './BinaryExpression';
import GroupExpression from './GroupExpression';
import FunctionExpression from './FunctionExpression';
import UnaryExpression from './UnaryExpression';

export default class ExpressionBuilder {
    public static literal(value: unknown): Expression {
        return new Value(value);
    }

    public static field(name: string): Expression {
        return new Field(name);
    }

    public static eq(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_EQUAL, right);
    }

    public static ne(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_NOT_EQUAL, right);
    }

    public static gt(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_GREATER_THAN, right);
    }

    public static ge(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_GREATER_THAN_OR_EQUAL, right);
    }

    public static lt(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_LOWER_THAN, right);
    }

    public static le(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_LOWER_THAN_OR_EQUAL, right);
    }

    public static has(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_HAS, right);
    }

    public static in(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_IN, right);
    }

    public static not(expression: Expression): Expression {
        return new UnaryExpression(expression, Constants.LOGICAL_NOT);
    }

    public static or(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_OR, right);
    }

    public static and(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.LOGICAL_AND, right);
    }

    public static group(expressions: Expression[], operator: Constants): Expression {
        return new GroupExpression(operator, expressions);
    }

    public static startsWith(left: Expression, right: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_STARTS_WITH, [left, right]);
    }

    public static endsWith(left: Expression, right: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_ENDS_WITH, [left, right]);
    }

    public static contains(left: Expression, right: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_CONTAINS, [left, right]);
    }

    public static concat(left: Expression, right: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_CONCAT, [left, right]);
    }

    public static indexOf(left: Expression, right: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_INDEX_OF, [left, right]);
    }

    public static lengthOf(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_LENGTH, [expression]);
    }

    public static substring(expression: Expression, length: number): Expression {
        return new FunctionExpression(Constants.FUNCTION_SUBSTRING, [expression, this.literal(length)]);
    }

    public static toLower(expression: Expression) {
        return new FunctionExpression(Constants.FUNCTION_TO_LOWER, [expression]);
    }

    public static toUpper(expression: Expression) {
        return new FunctionExpression(Constants.FUNCTION_TO_UPPER, [expression]);
    }

    public static matchesPattern(expression: Expression, pattern: string) {
        return new FunctionExpression(Constants.FUNCTION_MATCHES_PATTERN, [expression, this.literal(pattern)])
    }

    public static trim(expression: Expression) {
        return new FunctionExpression(Constants.FUNCTION_TRIM, [expression]);
    }

    public static floor(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_FLOOR, [expression]);
    }

    public static ceiling(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_CEILING, [expression]);
    }

    public static round(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_ROUND, [expression]);
    }

    public static date(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_DATE, [expression]);
    }

    public static day(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_DAY, [expression]);
    }

    public static hour(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_HOUR, [expression]);
    }

    public static minute(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_MINUTE, [expression]);
    }

    public static month(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_MONTH, [expression]);
    }

    public static second(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_SECOND, [expression]);
    }

    public static time(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_TIME, [expression]);
    }

    public static year(expression: Expression): Expression {
        return new FunctionExpression(Constants.FUNCTION_YEAR, [expression]);
    }

    public static add(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.ARITHMETIC_ADDITION, right);
    }

    public static sub(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.ARITHMETIC_SUBTRACTION, right);
    }

    public static neg(expression: Expression): Expression {
        return new UnaryExpression(expression, Constants.ARITHMETIC_NEGATION);
    }

    public static mul(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.ARITHMETIC_MULTIPLICATION, right);
    }

    public static div(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.ARITHMETIC_DIVISION, right);
    }

    public static mod(left: Expression, right: Expression): Expression {
        return new BinaryExpression(left, Constants.ARITHMETIC_MODULO, right);
    }

}
