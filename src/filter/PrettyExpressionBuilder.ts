import Expression, { Constants } from './Expression';
import ExpressionBuilder from './ExpressionBuilder';
import GroupExpression from './GroupExpression';

export default class PrettyExpressionBuilder {
    public static equal(field: string, value: any): Expression {
        return ExpressionBuilder.eq(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static notEqual(field: string, value: any): Expression {
        return ExpressionBuilder.ne(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static greaterThen(field: string, value: any): Expression {
        return ExpressionBuilder.gt(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static greaterThenOrEqual(field: string, value: any): Expression {
        return ExpressionBuilder.ge(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static lowerThen(field: string, value: any): Expression {
        return ExpressionBuilder.lt(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static lowerThenOrEqual(field: string, value: any): Expression {
        return ExpressionBuilder.le(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static in(field: string, value: any[]): Expression {
        return ExpressionBuilder.in(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static has(field: string, value: any) {
        return ExpressionBuilder.has(ExpressionBuilder.field(field), ExpressionBuilder.literal(value))
    }

    public static and(...expression: Expression[]): Expression {
        return new GroupExpression(Constants.LOGICAL_AND, expression);
    }

    public static or(...expression: Expression[]): Expression {
        return new GroupExpression(Constants.LOGICAL_OR, expression);
    }

    public static plus(field: string, value: number): Expression {
        return ExpressionBuilder.add(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static minus(field: string, value: number): Expression {
        return ExpressionBuilder.sub(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static times(field: string, value: number): Expression {
        return ExpressionBuilder.mul(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static divided(field: string, value: number): Expression {
        return ExpressionBuilder.div(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static modulo(field: string, value: number): Expression {
        return ExpressionBuilder.mod(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static startsWith(field: string, value: string): Expression {
        return ExpressionBuilder.startsWith(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static endsWith(field: string, value: string): Expression {
        return ExpressionBuilder.endsWith(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static contains(field: string, value: string): Expression {
        return ExpressionBuilder.contains(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static concat(field: string, value: string): Expression {
        return ExpressionBuilder.concat(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static indexOf(field: string, value: string): Expression {
        return ExpressionBuilder.indexOf(ExpressionBuilder.field(field), ExpressionBuilder.literal(value));
    }

    public static lengthOf(field: string): Expression {
        return ExpressionBuilder.lengthOf(ExpressionBuilder.field(field));
    }

    public static substring(field: string, length: number): Expression {
        return ExpressionBuilder.substring(ExpressionBuilder.field(field), length);
    }
}
