import Expression, { Constants } from './Expression';

export default class FunctionExpression implements Expression {
    private fn: Constants;
    private args: Expression[];

    constructor(fn: Constants, args: Expression[]) {
        this.fn = fn;
        this.args = args;
    }

    express(): string {
        let res = `${this.fn}(`;
        if (this.args.length > 0) {
            let comma = false;
            for (const arg of this.args) {
                if (comma) {
                    res += ',';
                }
                res += `${arg.express()}`;
                comma = true;
            }
        }
        res += `)`;
        return res;
    }
}
