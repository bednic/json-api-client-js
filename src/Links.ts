import Link from './Link';

export default interface Links {
    self?: string | Link;
    related?: string | Link;
    first?: string | Link;
    next?: string | Link;
    prev?: string | Link;
    last?: string | Link;
}
