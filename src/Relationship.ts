import Links from './Links';
import Meta from './Meta';
import ResourceIdentifier from './ResourceIdentifier';

export default interface Relationship {
    data: ResourceIdentifier | ResourceIdentifier[] | null;
    links?: Links;
    meta?: Meta;
}
