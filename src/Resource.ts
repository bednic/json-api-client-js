import ResourceIdentifier from './ResourceIdentifier';
import Relationship from './Relationship';

export default interface Resource extends ResourceIdentifier {
    attributes?: {
        [key: string]: any;
    };
    relationships?: {
        [key: string]: Relationship;
    };
}
