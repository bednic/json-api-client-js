import Resource from './Resource';
import ResourceIdentifier from './ResourceIdentifier';
import Meta from './Meta';
import Links from './Links';
import Relationship from './Relationship';
import Filter from './filter/Filter';
import ErrorDocument from './ErrorDocument';
import LinkIsEmpty from './error/LinkIsEmpty';
import InvalidArgument from "./error/InvalidArgument";
import Link from "./Link";
import NativeFetchClient from "./xhr/NativeFetchClient";

export default class CompoundDocument<T extends ResourceIdentifier> {
    public readonly jsonapi: { version: string } = {version: '1.0'};
    public data!: T | T[] | null;
    public meta?: Meta;
    public links?: Links;
    public included?: Resource[];

    private readonly endpoint!: string;
    private readonly criteria!: Filter;
    private readonly client!: Client;

    private params: { [key: string]: string } = {};

    constructor(endpoint: string, client?: Client) {
        this.endpoint = endpoint;
        this.criteria = new Filter();
        this.client = client ?? new NativeFetchClient();
        this.client.setHeader('Accept', 'application/vnd.api+json');
        this.client.setHeader('Content-Type', 'application/vnd.api+json');
    }

    /**
     * Cancel request
     */
    public abort(): void {
        this.client.abort();
    }

    /**
     * Create resource from passed data
     */
    public async create(data: Partial<T>): Promise<CompoundDocument<T>> {
        const req = JSON.stringify({
            jsonapi: this.jsonapi,
            meta: this.meta,
            data
        });
        const response = await this.client.post(this.endpoint + this.criteria.toQuery(), req);
        const body: any = await response.json();
        this.fillUp(body);
        return this;
    }

    /**
     * Fetching self data to document
     */
    public async self(): Promise<CompoundDocument<T>> {
        await this.fetch(this.endpoint + this.criteria.toQuery());
        return this;
    }

    /**
     * Update resources by passed data
     */
    public async update(data: Partial<T>): Promise<CompoundDocument<T>> {
        const req = JSON.stringify({
            jsonapi: this.jsonapi,
            meta: this.meta,
            data
        });
        const response = await this.client.patch(this.endpoint + this.criteria.toQuery(), req);
        const document: CompoundDocument<T> | ErrorDocument = await response.json();
        this.fillUp(document);
        return this;
    }

    /**
     * Make deleting request
     */
    public async delete(): Promise<void> {
        const response = await this.client.delete(this.endpoint);
        if (response.bodyUsed) {
            const document: any = await response.json();
            this.fillUp(document);
        } else {
            this.data = null;
            this.meta = undefined;
            this.links = undefined;
            this.included = undefined;
        }
    }

    /**
     * Fetch first page of document
     */
    public async first(): Promise<CompoundDocument<T>> {
        if (this.links?.first) {
            await this.fetch(CompoundDocument.getEndpoint(this.links.first));
        } else {
            throw new LinkIsEmpty('first');
        }
        return this;
    }

    /**
     * Fetch next page of document
     */
    public async next(): Promise<CompoundDocument<T>> {
        if (this.links?.next) {
            await this.fetch(CompoundDocument.getEndpoint(this.links.next));
        } else {
            throw new LinkIsEmpty('next');
        }
        return this;
    }

    /**
     * Fetch previous page of document
     */
    public async prev(): Promise<CompoundDocument<T>> {
        if (this.links?.prev) {
            await this.fetch(CompoundDocument.getEndpoint(this.links.prev));
        } else {
            throw new LinkIsEmpty('prev');
        }
        return this;
    }

    /**
     * Fetch last page of document
     */
    public async last(): Promise<CompoundDocument<T>> {
        if (this.links?.last) {
            await this.fetch(CompoundDocument.getEndpoint(this.links.last));
        } else {
            throw new LinkIsEmpty('last');
        }
        return this;
    }

    /**
     * Returns document of ResourceIdentifiers for provided relationship
     * @param relationship
     */
    public getRelationship<R extends ResourceIdentifier>(relationship: Relationship): CompoundDocument<R> {
        return new CompoundDocument<R>(CompoundDocument.getEndpoint(relationship.links?.self), this.client);
    }

    /**
     * Returns document of Resources for provider relationship
     * @param relationship
     */
    public getRelation<R extends Resource>(relationship: Relationship): CompoundDocument<R> {
        return new CompoundDocument<R>(CompoundDocument.getEndpoint(relationship.links?.related), this.client);
    }

    /**
     * Provide access to document filter
     */
    public filter() {
        return this.criteria;
    }

    /**
     * Add custom parameter to request query part
     * @param name
     * @param value
     */
    public addCustomQueryParam(name: string, value: string) {
        this.params[name] = value;
    }

    private async fetch(url: string): Promise<void> {
        const response = await this.client.get(url + this.criteria.toQuery());
        const document: CompoundDocument<T> | ErrorDocument = await response.json();
        this.fillUp(document);
    }

    private fillUp(data: CompoundDocument<T> | ErrorDocument): void {
        if (data instanceof ErrorDocument) {
            throw data;
        } else {
            this.data = data.data;
            this.links = data.links;
            this.meta = data.meta;
            this.included = data.included;
        }
    }

    private static getEndpoint(link: string | Link | null | undefined): string {
        if (link === undefined || link === null) {
            throw new InvalidArgument('link', 'string or Link', typeof link);
        } else if (typeof link === 'string') {
            return link
        } else {
            return link.href;
        }
    }
}
