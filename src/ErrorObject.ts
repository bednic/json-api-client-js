import Meta from './Meta';

export default interface ErrorObject{
    id?: string;
    links?: { about: string };
    status?: string;
    code?: string;
    title?: string;
    detail?: string;
    source?: { pointer?: string; parameter?: string };
    meta?: Meta;
}
